### This file is part of the Savu Scheduler application
### Copyright 2011 Fletcher Haynes

### Licensed under the Apache License, Version 2.0 (the "License");
### you may not use this file except in compliance with the License.
### You may obtain a copy of the License at
### 
###     http://www.apache.org/licenses/LICENSE-2.0
### 
### Unless required by applicable law or agreed to in writing, software
### distributed under the License is distributed on an "AS IS" BASIS,
### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
### See the License for the specific language governing permissions and
### limitations under the License.

from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

import customerconstants
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^scheduler/', include('scheduler.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     (r'^admin/', include(admin.site.urls)),
     
     
     # AJAX Views
     (r'^ajaxLogin/', 'schedulemanager.views.login'),
     (r'^doAjaxLogin/', 'schedulemanager.views.doAjaxLogin'),
     (r'^ajaxHome/', 'schedulemanager.views.ajaxHome'),
     (r'^ajaxGetNews/', 'schedulemanager.views.ajaxGetNews'),
     (r'^ajaxViewSchedule/', 'schedulemanager.views.ajaxViewSchedule'),
     (r'^ajaxLoadCalendar/', 'schedulemanager.views.ajaxLoadCalendar'),
     (r'^ajaxClaimShifts/', 'schedulemanager.views.ajaxClaimShifts'),
     (r'^ajaxDropShifts/', 'schedulemanager.views.ajaxDropShifts'),
     (r'^ajaxAssignShifts/', 'schedulemanager.views.ajaxAssignShifts'),
     (r'^ajaxGetOpenShifts/', 'schedulemanager.views.ajaxGetOpenShifts'),
     (r'^ajaxManageAvailability/', 'schedulemanager.views.ajaxManageAvailability'),
     (r'^ajaxGetNotes/', 'schedulemanager.views.ajaxGetNotes'),
     (r'^ajaxAddNote/', 'schedulemanager.views.ajaxAddNote'),
     (r'^ajaxGetOptions/', 'schedulemanager.views.ajaxGetOptions'),
     (r'^ajaxEditShiftOptions/', 'schedulemanager.views.ajaxEditShiftOptions'),
     (r'^ajaxLoadAvailability/', 'schedulemanager.views.ajaxLoadAvailability'),
     (r'^ajaxResetAvailability/', 'schedulemanager.views.ajaxResetAvailability'),
     (r'^ajaxResetAllAvailability/', 'schedulemanager.views.ajaxResetAllAvailability'),
     (r'^ajaxChangeAvailability/', 'schedulemanager.views.ajaxChangeAvailability'),
     (r'^ajaxManageProfile/', 'schedulemanager.views.ajaxManageProfile'),
     (r'^ajaxUpdateProfile/', 'schedulemanager.views.ajaxUpdateProfile'),
     (r'^ajaxGetAvailableWorkers/', 'schedulemanager.views.ajaxGetAvailableWorkers'),
     (r'^ajaxAssignWorkerToShift/', 'schedulemanager.views.ajaxAssignWorkerToShift'),
     (r'^ajaxEditLocations/', 'schedulemanager.views.ajaxEditLocations'),
     (r'^ajaxGetLocations/', 'schedulemanager.views.ajaxGetLocations'),
     (r'^ajaxLogout/', 'schedulemanager.views.ajaxLogout'),
     (r'^password_reset/$', 'django.contrib.auth.views.password_reset'),
     (r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done'),
     (r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm'),
     (r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete'),
     (r'^ajaxModifyLocation/', 'schedulemanager.views.ajaxModifyLocation'),
     (r'^ajaxEditShifts/', 'schedulemanager.views.ajaxEditShifts'),
     (r'^ajaxEditShift/', 'schedulemanager.views.ajaxEditShift'),
     (r'^ajaxViewAllAvailabilities/', 'schedulemanager.views.ajaxViewAllAvailabilities'),
     (r'^ajaxViewChangeShiftRequests/', 'schedulemanager.views.ajaxViewChangeShiftRequests'),
     (r'^ajaxGetChangeShiftRequests/', 'schedulemanager.views.ajaxGetChangeShiftRequests'),
     (r'^ajaxProcessChangeRequest/', 'schedulemanager.views.ajaxProcessChangeRequest'),
     (r'^ajaxGetTargetProfile/', 'schedulemanager.views.ajaxGetTargetProfile'),
     (r'^ajaxRegister/', 'schedulemanager.views.ajaxRegister'),
     (r'^ajaxRegisterNew/', 'schedulemanager.views.ajaxRegisterNew'),
     (r'^getIcal/', 'schedulemanager.views.outputCalendar'),
     (r'^ajaxGetIcalString/', 'schedulemanager.views.getIcalString'),
     (r'^$', 'schedulemanager.views.ajaxHome'),
     (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': customerconstants.installPath+'static/'}),
)

if customerconstants.useCAS:
    urlpatterns += patterns('',
        (r'^accounts/login/$', 'django_cas.views.login'),
        (r'^accounts/logout/$', 'django_cas.views.logout'),
        )
