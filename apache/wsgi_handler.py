import os, sys
sys.path.append('/usr/local/django-applications/')
sys.path.append('/usr/local/django-applications/savu-scheduler/')
import customerconstants
os.environ['DJANGO_SETTINGS_MODULE'] = customerconstants.wsgiDjangoSettingsString
import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()
