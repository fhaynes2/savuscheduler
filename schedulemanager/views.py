### This file is part of the Savu Scheduler application
### Copyright 2011 Fletcher Haynes

### Licensed under the Apache License, Version 2.0 (the "License");
### you may not use this file except in compliance with the License.
### You may obtain a copy of the License at
### 
###     http://www.apache.org/licenses/LICENSE-2.0
### 
### Unless required by applicable law or agreed to in writing, software
### distributed under the License is distributed on an "AS IS" BASIS,
### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
### See the License for the specific language governing permissions and
### limitations under the License.
# Django function imports
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import check_password
from django.contrib.auth.decorators import login_required
from django.utils import simplejson

# Model Imports
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from schedulemanager.models import UserProfile, Location, MicroShift, OverShift, NewsPost, Note, AppPreferences, ChangeRequest
from schedulemanager.models import IndividualChangeRequest


# Template and Django utility imports
from django.template import Context, RequestContext, loader
from django.core import serializers

# Non-Django imports
import datetime
import pdb, json
import icalendar

# Custom module imports
import timeProcessor
import datetimedicts
import utility
import timezones


# AJAX Views

def json_response(thing):
    '''This function takes Python data structure and returns it JSON formatted. It is best to pass dictionaries to it.'''
    return HttpResponse(simplejson.dumps(thing), mimetype="application/json")
                         
def login(request):
    '''This handles showing the initial login page.'''
    t = loader.get_template('ajax_login.html')
    html = t.render(Context({}))
    return HttpResponse(html)

def doAjaxLogin(request):
    '''This processes the login POST request from the login page.'''
    username = request.POST['uname']
    password = request.POST['pass']
    user = authenticate(username=username, password=password)
    
    # Is the user valid?
    if user is not None:
        # Is the user active?
        if user.is_active:
            auth_login(request, user)
        else:
            return json_response({"resultString":'expired'})
    else:
        # Return invalid login message
        return HttpResponse(simplejson.dumps({'resultString':'invalid'}), mimetype="application/json")
    return json_response({'resultString':'ok'})

@login_required
def ajaxHome(request):
    '''Renders and displays the main index/home page.'''
    t = loader.get_template("ajax_home.html")
    html = t.render(RequestContext(request, {}))
    return HttpResponse(html)

@login_required 
def ajaxGetNews(request):
    '''AJAX Function to get all the news posts, ordered by date.'''
    today = datetime.datetime.now()
    newsItems = MicroShift.objects.filter(showOnNews=True, status="Unassigned").order_by('date')
    # Update this to use json_response
    data = serializers.serialize("json", newsItems)
    return HttpResponse(data, mimetype="application/json")

    
@login_required
def ajaxGetOpenShifts(request):
    '''AJAX function to get all the unassigned shifts that should be displayed on the news page'''
    today = datetime.datetime.now()
    openShiftsQs = MicroShift.objects.filter(showOnNews=True, worker=None, date__gte=today).order_by('date')
    openShifts = []
    for eachShift in openShiftsQs:
        openShifts.append(eachShift)
    results = []
    resultBlock = []
    for eachShift in openShifts:
        shiftToCheck = eachShift
        unassignedDayShifts = MicroShift.objects.filter(showOnNews=True, worker=None, location=shiftToCheck.location, date=shiftToCheck.date).order_by('startingTime')
        dayList = []
        for x in unassignedDayShifts:
            dayList.append(x)
        while len(dayList) != 0:
            if len(resultBlock) == 0:
                lastShift = dayList[0]
                if dayList[0] in openShifts:
                    openShifts.remove(dayList[0])
                resultBlock.append(dayList.pop(0))
                if len(dayList) == 0:
                    results.append(resultBlock)
            else:
                if dayList[0].startingTime == lastShift.endingTime:
                    lastShift = dayList[0]
                    if dayList[0] in openShifts:
                        openShifts.remove(dayList[0])
                    resultBlock.append(dayList.pop(0))
                    futureCheck = MicroShift.objects.filter(showOnNews=True, worker=None, location=lastShift.location, startingTime=lastShift.endingTime)
                    if not futureCheck:
                        results.append(resultBlock)
                        resultBlock = []
                else:
                    results.append(resultBlock)
                    resultBlock = []
    htmlResults = []
    for x in results:
        if len(x) == 1:
            hoursInShift = "30 minute"
        else:
            hoursInShift = str(len(x)/2)+" hour"
        htmlResults.append('<div class="newsEntry"><div class="newsHeader">')
        htmlResults.append('<form action="/ajaxClaimShifts/" method="post">')
        for y in x:
            htmlResults.append('<input type="hidden" name="'+str(y.id)+'" value="'+str(y.id)+'" />')
        
        htmlResults.append('There is a '+hoursInShift+' shift available at '+x[0].location.locationName+'. See below for details. Please click Claim to claim all for them.<button class="submitClaimButton" type="submit">Claim</button></div>\r\n')
        htmlResults.append('<div class="newsBody">')
        for y in x:
            htmlResults.append(y.date.strftime("%B %d")+' '+y.startingTime.strftime("%I:%M %p")+' to '+y.endingTime.strftime("%I:%M %p")+'<br />')
        htmlResults.append('</div></div></form>')
    htmlResults = "".join(htmlResults)
    return HttpResponse(htmlResults)
            
        
@login_required
def ajaxGetLocations(request):
    '''AJAX function to get all JSON object holding all locations.'''
    locations = Location.objects.all()
    data = serializers.serialize("json", locations)
    return HttpResponse(data, mimetype="application/json")

@login_required
def ajaxManageAvailability(request):
    '''Renders and displays the users Manage Availability screen.'''
    
    # The first row of the days of the week, offset to compensate for the time block section
    firstRow = []
    profile = UserProfile.objects.get(user__username=request.user.username)
    firstRow.append("<tr>")
    firstRow.append("<td id='loadedUser'>"+profile.user.username+"</td>")
    for eachDay in datetimedicts.days:
        firstRow.append('<td>'+eachDay+'</td>')
    firstRow.append("</tr>")
    
    # Availability rows holds each line of HTML as we build it that we'll later join together
    availabilityRows = []
    availabilityRows.append("".join(firstRow))
    i = 0
    
    # We're going to build the table one row at a time, starting with the first one in the datetimedicts.times list
    # We have one row per half hour block
    for eachTime in datetimedicts.times:
        curRow = []
        curRow.append("<tr>")
        try:
            # We don't care about 2359-0000.
            if eachTime == "2359":
                pass
            else:
                
                curRow.append('<td>'+utility.twentyFourToTwelve(eachTime)+'-'+utility.twentyFourToTwelve(datetimedicts.times[i+1])+'</td>')
        except:
            # If we throw an exception (due to i+1 not being a valid subscript), we know we've reached the end and to start back at the beginning
            curRow.append('<td>'+utility.twentyFourToTwelve(eachTime)+'-'+utility.twentyFourToTwelve(datetimedicts.times[0])+'</td>')
            
        # Now we'll check each day of each block. Monday 8-8:30, Tuesday 8-8:30, etc. This lets us build the table horizontally.    
        for eachDay in datetimedicts.days:
            # same deal as above with the try/except. When we throw an error, we know we've reached the end of the list.
            try:
                status = getattr(profile, eachDay.lower()+eachTime+'to'+datetimedicts.times[i+1])
            except:
                try:
                    status = getattr(profile, eachDay.lower()+eachTime+'to'+datetimedicts.times[0])
                except:
                    # This should never be reached
                    status = "Unknown"
            
            # Now we'll build a td for the 4 availability types.
            if status.lower() == 'Available'.lower():
                try:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[i+1]+'" class="available availabilityblock">'+status+'</td>')
                except:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[0]+'" class="available availabilityblock">'+status+'</td>')
            elif status.lower() == 'Unavailable'.lower():
                try:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[i+1]+'" class="unavailable availabilityblock">'+status+'</td>')
                except:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[0]+'" class="unavailable availabilityblock">'+status+'</td>')
            elif status.lower() == 'Preferred'.lower():
                try:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[i+1]+'" class="preferred availabilityblock">'+status+'</td>')
                except:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[0]+'" class="preferred availabilityblock">'+status+'</td>')
            elif status.lower() == 'Not Preferred'.lower() or status.lower() == 'Notpreferred':
                try:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[i+1]+'" class="notpreferred availabilityblock">'+status+'</td>')
                except:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[0]+'" class="notpreferred availabilityblock">'+status+'</td>')
            else:    
                #curRow.append('<td>'+status+'</td>')
                pass
        i+=1
        
        # End the row. We repeat this for every half hour block.
        curRow.append("</tr>")
        availabilityRows.append("".join(curRow))
    
    t = loader.get_template('ajax_manage_availability.html')
    availabilityHtml = "".join(availabilityRows)
    request.user.get_profile().color
    # We use the templating system to inject the HTML directly into the page before it is served. We could probably AJAXify this to make it more consistent.
    html = t.render(RequestContext(request, {'availabilityDiv':availabilityHtml, 'profile':request.user.get_profile(), 'users':User.objects.all().order_by('username'), 'requestMaker':request.user}))
    return HttpResponse(html)

@login_required
def ajaxChangeAvailability(request):
    '''
    Handles AJAX request to change a user's availability. The page sends a list td IDs to change,
    and we use setattr to find the corresponding field in the user profile.
    '''
    profile = UserProfile.objects.get(user__username=request.POST['targetWorker'])
    for eachKey in request.POST.keys():
        try:
            
            day, start, end = eachKey.split("-")
            #print profile.user.username+": "+getattr(profile, day.lower()+start+'to'+end, request.POST[eachKey])
            setattr(profile, day.lower()+start+'to'+end, request.POST[eachKey])
            #print profile.user.username+": "+getattr(profile, day.lower()+start+'to'+end, request.POST[eachKey])
            profile.save()
        except ValueError:
            # This should never be reached...if we do, corrupt data.
            pass
    return HttpResponse("OK")

@login_required
def ajaxViewAllAvailabilities(request):
    '''Shows the availabilities of all users as one gigantic table.'''
    
    
    users = User.objects.all()
    
    # We need this to determine some offsets
    userCount = len(users)
    
    firstRow = []
    secondRow = []
    availabilityRows = []
    
    # We append an empty string for offset purposes
    firstRow.append("")
    secondRow.append("")
    
    # We'll build the rows, since we can calculate how many "sections" well need. 7 days per user, with a column for times.
    for eachUser in users:
        firstRow.append(eachUser.username)
        for x in range(0, 7):
            firstRow.append("")
        for eachDay in datetimedicts.days:
            secondRow.append(eachDay)
        secondRow.append("")
    i = 0
    
    # Build each table horizontally again, same as in the single availability view.
    for eachTime in datetimedicts.times:
        curRow = []
        for eachUser in users:
            try:
                if eachTime == "2359":
                    pass
                else:
                    curRow.append('<td>'+utility.twentyFourToTwelve(eachTime)+'-'+utility.twentyFourToTwelve(datetimedicts.times[i+1])+'</td>')
            except:
                curRow.append('<td>'+utility.twentyFourToTwelve(eachTime)+'-'+utility.twentyFourToTwelve(datetimedicts.times[0])+'</td>')
            profile = eachUser.get_profile()
            for eachDay in datetimedicts.days:
                try:
                    status = getattr(profile, eachDay.lower()+eachTime+'to'+datetimedicts.times[i+1])
                except:
                    try:
                        status = getattr(profile, eachDay.lower()+eachTime+'to'+datetimedicts.times[0])
                    except:
                        status = "Unknown"

                if status == 'Available':
                    curRow.append('<td class="available">'+status+'</td>')
                elif status == 'Unavailable':
                    curRow.append('<td class="unavailable">'+status+'</td>')
                elif status == 'Preferred':
                    curRow.append('<td class="preferred">'+status+'</td>')
                elif status == 'Not Preferred':
                    curRow.append('<td class="notpreferred">'+status+'</td>')
                else:    
                    #curRow.append('<td>'+status+'</td>')
                    pass
        i+=1
        availabilityRows.append(curRow)
    
    t = loader.get_template('ajax_view_all_availabilities.html')
    html = t.render(RequestContext(request, {'availabilityRows':availabilityRows, 'firstRow':firstRow, 'secondRow':secondRow}))
    return HttpResponse(html)

@login_required
def ajaxLoadAvailability(request):
    '''
    Responds to AJAX request for a specific user's availability. If targetWorker is
    in the request, it uses that. If not, it defaults to the person making the request.
    '''
    
    firstRow = []
    # If the page specified a user, we'll use that. If not, default to user making request.
    # TODO: Should check to make sure the requestor is a supervisor, even though they only
    # see the targetWorker box on the page if they are a supervisor. Otherwise, someone
    # could craft a malicious URL to do this.
    if 'targetWorker' in request.POST:
        profile = UserProfile.objects.get(user__username=request.POST['targetWorker'])
    else:
        profile = UserProfile.objects.get(user__username=request.user.username)
        
    firstRow.append("<table id='availabilityTable'><tr>")
    firstRow.append("<td id='loadedUser'>"+profile.user.username+"</td>")
    for eachDay in datetimedicts.days:
        firstRow.append('<td>'+eachDay+'</td>')
    firstRow.append("</tr>")
    availabilityRows = []
    availabilityRows.append("".join(firstRow))
    i = 0
    
    for eachTime in datetimedicts.times:

        curRow = []
        curRow.append("<tr>")
        try:
            if eachTime == "2359":
                pass
            else:
                curRow.append('<td>'+utility.twentyFourToTwelve(eachTime)+'-'+utility.twentyFourToTwelve(datetimedicts.times[i+1])+'</td>')
        except:
            curRow.append('<td>'+utility.twentyFourToTwelve(eachTime)+'-'+utility.twentyFourToTwelve(datetimedicts.times[0])+'</td>')
        for eachDay in datetimedicts.days:
            try:
                status = getattr(profile, eachDay.lower()+eachTime+'to'+datetimedicts.times[i+1])
            except:
                try:
                    status = getattr(profile, eachDay.lower()+eachTime+'to'+datetimedicts.times[0])
                except:
                    status = "Unknown"
            if status == 'Available':
                try:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[i+1]+'" class="available availabilityblock">'+status+'</td>')
                except:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[0]+'" class="available availabilityblock">'+status+'</td>')
            elif status.lower() == 'Unavailable'.lower():
                try:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[i+1]+'" class="unavailable availabilityblock">'+status+'</td>')
                except:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[0]+'" class="unavailable availabilityblock">'+status+'</td>')
            elif status.lower() == 'Preferred'.lower():
                try:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[i+1]+'" class="preferred availabilityblock">'+status+'</td>')
                except:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[0]+'" class="preferred availabilityblock">'+status+'</td>')
            elif status.lower() == 'Not Preferred'.lower() or status.lower() == 'Notpreferred':
                try:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[i+1]+'" class="notpreferred availabilityblock">'+status+'</td>')
                except:
                    curRow.append('<td id="'+eachDay+'-'+eachTime+'-'+datetimedicts.times[0]+'" class="notpreferred availabilityblock">'+status+'</td>')
            else:    
                #curRow.append('<td>'+status+'</td>')
                pass
        i+=1
        curRow.append("</tr>")
        availabilityRows.append("".join(curRow))
    availabilityRows.append("</table>")
    return HttpResponse("".join(availabilityRows))
    

@login_required
def ajaxResetAvailability(request):
    i = 0

    profile = UserProfile.objects.get(user__username=request.POST['targetWorker'])
    for eachTime in datetimedicts.times:
        for eachDay in datetimedicts.days:
            try:
                setattr(profile, eachDay.lower()+eachTime+'to'+datetimedicts.times[i+1], "Unavailable")
            except:
                try:
                    setattr(profile, eachDay.lower()+eachTime+'to'+datetimedicts.times[0], "Unavailable")
                except:
                    pass
        i+=1
    request.user.save()
    profile.save()
    html = ajaxLoadAvailability(request)
    return HttpResponse(html)

@login_required
def ajaxResetAllAvailability(request):
    return HttpResponse("Function not yet implemented!")
    '''
    for eachUser in User.objects.all():
        i = 0
        profile = UserProfile.objects.get(user__username=eachUser.username)
        for eachTime in datetimedicts.times:
            for eachDay in datetimedicts.day:
                try:
                    setattr(profile, eachDay.lower()+eachTime+"to"+datetimedicts.times[i+1], "Unavailable")
                except:
                    try:
                        setattr(profile, eachDay.lower()+eachTime+"to"+datetimedicts.times[0], "Unavailable")
                    except:
                        pass
            i+=1
        eachUser.save()
        profile.save()
    html = ajaxLoadAvailability(request)
    return HttpResponse(html)
   ''' 
@login_required
def ajaxManageProfile(request):
    '''Renders and displays the manage profile page.'''
    t = loader.get_template("ajax_manage_profile.html")
    users = User.objects.all()
    current_site = Site.objects.get_current()
    html = t.render(RequestContext(request, {'user':request.user, 'users':users, 'locations':Location.objects.all(), 'sitename':current_site.domain}))
    return HttpResponse(html)

@login_required
def ajaxGetTargetProfile(request):
    '''Responds to an AJAX request for a specific user's profile info. Returns a JSON object.'''
    targetWorker = User.objects.get(username=request.GET['targetWorker'])
    responseDict = {}
    responseDict['firstName'] = targetWorker.first_name
    responseDict['lastName'] = targetWorker.last_name
    responseDict['email'] = targetWorker.email
    responseDict['foreground'] = targetWorker.get_profile().textColor
    responseDict['background'] = targetWorker.get_profile().color
    return json_response(responseDict)
    
@login_required
def ajaxViewSchedule(request):
    '''Renders and displays the initial schedule page.'''
    #locationNames = utility.getLocationNames()

    #numLocs = len(locationNames)
    
    users = User.objects.all().order_by("username")
    t = loader.get_template('ajax_view_schedule.html')
    # We include this in the context in case they are a supervisor in order to populate the
    # assign worker drop down
    html = t.render(RequestContext(request, {"users":users}))
    return HttpResponse(html)
    
@login_required
def ajaxLoadCalendar(request):
    '''Responds to an AJAX request to load one or more days of the schedule.'''
    
    oneDay = datetime.timedelta(days=1)
    # If no date is specified, we defalt to today
    if not 'dateToLoad' in request.POST:
        targetStartDate = datetime.datetime.now()
        targetEndDate = datetime.datetime.now()
    else:
        targetMonth, targetDay, targetYear = request.POST['dateToLoad'].split("/")
        targetStartDate = datetime.date(int(targetYear), int(targetMonth), int(targetDay))
        
        # If they want to load more than one day at a time, and have put something in the
        # box on the page...
        if 'daysInAdvance' in request.POST:
            daysInAdvance = int(request.POST['daysInAdvance'])
            endDelta = datetime.timedelta(days=daysInAdvance)
            targetEndDate = targetStartDate + endDelta
        else:
            targetEndDate = datetime.date(int(targetYear), int(targetMonth), int(targetDay))
            

        
    # We'll need these for offsets and such later
    locationNames = utility.getLocationNames()
    locations = Location.objects.all()
    numLocs = len(locationNames)
    
    
    firstCalendarRow = utility.createFirstRow(targetStartDate, targetEndDate, numLocs)

    secondCalendarRow = utility.createSecondRow(targetStartDate, targetEndDate, locations)

    # Our cur date is our "counter" date object that we increase as we process each day
    curDate = targetStartDate
    
    # This will hold our HTML for our calendar as we build it
    calendarRows = []
    
    i = 0
    
    # We build it horizontally, going from left to right (the days) and then downwards (the half hour blocks)
    for eachTime in datetimedicts.times:
        calendarRows.append('<tr>')
 
        if eachTime == '2359':
            pass
        else:
            curRow = []
            curDate = targetStartDate
            
    
            timeObj = utility.parseTime(eachTime)
            shifts = MicroShift.objects.filter(startingTime=timeObj)
            locations = Location.objects.all()
    
            # We want to get all the shifts for each day, so we loop until we reach the end date
            while curDate <= targetEndDate:

                shiftsOnThisDate = shifts.filter(date=curDate)
                # Each day is prefaced with a column of time blocks. Here we build that part...
                try:
                    calendarRows.append('<td class="timeblock">'+utility.twentyFourToTwelve(eachTime)+'-'+utility.twentyFourToTwelve(datetimedicts.times[i+1])+"</td>")
                except:
                    calendarRows.append('<td class="timeblock">'+utility.twentyFourToTwelve(eachTime)+'-'+utility.twentyFourToTwelve(datetimedicts.times[0])+"</td>")
                
                # Now we'll need to get any shift at that particular half hour block for each location
                for eachLocation in locations:
                    try:
                        shiftAtThisTime = shiftsOnThisDate.get(location=eachLocation)
                        try:
                            calendarRows.append('<td id="'+str(shiftAtThisTime.id)+'" class="'+shiftAtThisTime.worker.username+' scheduledblock"><a class="noteLink" id="'+str(shiftAtThisTime.id)+'" href="#">'+shiftAtThisTime.worker.username+"</a></td>")
                        except AttributeError:
                            calendarRows.append('<td id="'+str(shiftAtThisTime.id)+'" class="unassigned scheduledblock"><a class="noteLink" id="'+str(shiftAtThisTime.id)+'" href="#">Unassigned</a></td>')
                    except:
                        
                        calendarRows.append('<td>No Shift</td>')
                curDate += oneDay
        
        calendarRows.append('</tr>')       
        i+=1
    # Final assembly of the calendar.
    # TODO: Clean this up
    html = '<table id="calendarTable">'
    firstRowHtml = '<tr>'+"".join(firstCalendarRow)+'</tr>'
    secondRowHtml = '<tr>'+"".join(secondCalendarRow)+'</tr>'
    calendarBodyHtml = "".join(calendarRows)
    html += firstRowHtml + secondRowHtml + calendarBodyHtml
    html += '</table>'
    
    return HttpResponse(html)

@login_required  
def ajaxClaimShifts(request):
    '''Responds to an AJAX request to claim selected shifts.'''
    # The appPrefObject is a hacky way of setting sitewide preferences.
    # In this instance, we are checking of the user has it set so that
    # supervisors have to approve all drop/claim shift requests. We'll
    # need a reference to it to check it later on.
    
    appPrefObject = AppPreferences.objects.get(id=1)
    
    # If endClaimDate is in the request, we know it is a multi-day request
    if "endClaimDate" in request.POST:
        daysToRepeat = []
        endClaimDate = request.POST['endClaimDate']

        # If they checked any of the repeat boxes, we want to append that to a list
        # of days to repeat the claim
        if "rMonday" in request.POST:
            daysToRepeat.append('monday')
        if "rTuesday" in request.POST:
            daysToRepeat.append('tuesday')
        if "rWednesday" in request.POST:
            daysToRepeat.append('wednesday')
        if "rThursday" in request.POST:
            daysToRepeat.append('thursday')
        if "rFriday" in request.POST:
            daysToRepeat.append('friday')
        if "rSaturday" in request.POST:
            daysToRepeat.append('saturday')
        if "rSunday" in request.POST:
            daysToRepeat.append('sunday')
            
        
        if appPrefObject.requireShiftChangeApproval == True:
                # We need to assemble all possibly affected shifts to send to the utility
                # function that generates a bulk claim.
                shiftsToAddToRequest = []
                for eachKey in request.POST.keys():
                    # The page sends a bunch of IDs that are shift IDs - the selected ones, but we also get
                    # other info, such as endClaimDate. However, any key that is all digits must be a shift ID
                    if eachKey.isdigit():
                        shiftsToAddToRequest.append(eachKey)
                # Send everything we need to generate the bulk claim request...
                utility.generateMultiDayShiftClaimRequest(shiftsToAddToRequest, request.user, endClaimDate, daysToRepeat)
        else:
            for eachKey in request.POST.keys():
                if eachKey.isdigit():
                    # Similar to above, but we just claim them immediately and don't bother
                    # generating a bulk request.
                    utility.processMultiDayShiftClaim(eachKey, request.user, endClaimDate, daysToRepeat)
                else:
                    pass
        result = "ok"
    else:
        # If there is no end date, we just do an immediate claim.
        # TODO: Put in code to generate bulk requests for non-multiday claims
        if appPrefObject.requireShiftChangeApproval == True:
            shiftsToAddToRequest = []            
            for eachKey in request.POST.keys():
                if eachKey.isdigit():                    
                    shiftsToAddToRequest.append(eachKey)            
            utility.generateSingleDayShiftClaimRequest(shiftsToAddToRequest, request.user)
        else:
            for eachKey in request.POST.keys():
                if eachKey.isdigit():                    
                    tempResult = utility.processShiftClaim(eachKey, request.user)                   
                if tempResult != "ok":
                    result = tempResult
                else:
                    pass
        result = "ok"
    return HttpResponse(result)
    
@login_required   
def ajaxDropShifts(request):
    # See comments in above function. This functions almost identically.
    # TODO: could probably combine these two functions.
    appPrefObject = AppPreferences.objects.get(id=1)
    if "endDropDate" in request.POST:
        daysToRepeat = []
        endDropDate = request.POST['endDropDate']

        
        if "rMonday" in request.POST:
            daysToRepeat.append('monday')
        
        if "rTuesday" in request.POST:
            daysToRepeat.append('tuesday')
        
        if "rWednesday" in request.POST:
            daysToRepeat.append('wednesday')
         
        if "rThursday" in request.POST:
            daysToRepeat.append('thursday')
         
        if "rFriday" in request.POST:
            daysToRepeat.append('friday')
        if "rSaturday" in request.POST:
            daysToRepeat.append('saturday')
        if "rSunday" in request.POST:
            daysToRepeat.append('sunday')
                
        if appPrefObject.requireShiftChangeApproval == True:
            shiftsToAddToRequest = []
            for eachKey in request.POST.keys():
                if eachKey.isdigit():
                    #print eachKey
                    shiftsToAddToRequest.append(eachKey)
            utility.generateMultiDayShiftDropRequest(shiftsToAddToRequest, request.user, endDropDate, daysToRepeat)
        else:
            for eachKey in request.POST.keys():
                if eachKey.isdigit():
                    utility.processMultiDayShiftDrop(eachKey, request.user, endDropDate, daysToRepeat)
                else:
                    pass
        result = "ok"
    else:
        if appPrefObject.requireShiftChangeApproval == True:
            shiftsToAddToRequest = []
            for eachKey in request.POST.keys():
                if eachKey.isdigit():
                    shiftsToAddToRequest.append(eachKey)
            utility.generateSingleDayShiftDropRequest(shiftsToAddToRequest, request.user)
        else:
            for eachKey in request.POST.keys():
                if eachKey.isdigit():
                    tempResult = utility.processShiftDrop(eachKey, request.user)
                if tempResult != "ok":
                    result = tempResult
                else:
                    pass
        result = "ok"
    return HttpResponse(result)

@login_required
def ajaxAssignShifts(request):
    '''Handles AJAX request from a supervisor to assign a worker to shfits.'''
    
    # See comments for ajaxClaimShifts. It functions almost identically, but there
    # bulk claim generation or checking if the user is allowed, etc. It just assigns it.
    if request.POST['targetWorker'] == "unassigned":
        targetWorker = None
    else:
        targetWorker = User.objects.get(username=request.POST['targetWorker'])
        

    if "endAssignDate" in request.POST:
        daysToRepeat = []
        endAssignDate = request.POST['endAssignDate']

        
        if "rMonday" in request.POST:
            daysToRepeat.append('monday')
        
        if "rTuesday" in request.POST:
            daysToRepeat.append('tuesday')
        
        if "rWednesday" in request.POST:
            daysToRepeat.append('wednesday')
         
        if "rThursday" in request.POST:
            daysToRepeat.append('thursday')
         
        if "rFriday" in request.POST:
            daysToRepeat.append('friday')
        if "rSaturday" in request.POST:
            daysToRepeat.append('saturday')
        if "rSunday" in request.POST:
            daysToRepeat.append('sunday')
       
        for eachKey in request.POST.keys():
            if eachKey.isdigit():
                utility.processMultiDayShiftAssign(eachKey, targetWorker, endAssignDate, daysToRepeat)
            else:
                pass
        result = "ok"
    else:
        result = "ok"
        for eachKey in request.POST.keys():
            if eachKey.isdigit():
                tempResult = utility.processShiftAssign(eachKey, targetWorker)
                if tempResult != "ok":
                    result = tempResult
            else:
                pass
    return HttpResponse(result)

@login_required
def ajaxGetOptions(request):
    '''
    Responds to AJAX request to get all of the options for a shift.
    '''
    shift = MicroShift.objects.get(id=request.GET['shiftId'])
    options = {}
    if shift.showOnNews == True:
        options['showOnNews'] = True
    else:
        options['showOnNews'] = False
    optionsHtml = []
    optionsHtml.append('<form id="editOptionsForm" action="/ajaxEditShiftOptions/" method="post">\r\n')
    optionsHtml.append('<table id="editOptionsTable">\r\n')
    optionsHtml.append('<tr>\r\n')
    optionsHtml.append('<td><label for="showOnNews">Show on News:</label></td>\r\n')
    if options['showOnNews'] == True:
        optionsHtml.append('<td><input type="checkbox" name="showOnNews" checked="yes" id="showOnNews"/></td>\r\n')
    else:
        optionsHtml.append('<td><input type="checkbox" name="showOnNews" id="showOnNews" /></td>\r\n')
    optionsHtml.append('</tr>')
    optionsHtml.append('<tr>')
    optionsHtml.append('<td class="formLabel">Reoccur:</td>\r\n')
    optionsHtml.append('<td class="formInput" class="repeatTdOne">\r\n')
    optionsHtml.append('<input type="checkbox" id="roAll" name="roAll" value="true" />All<br />\r\n')
    optionsHtml.append('<input type="checkbox" id="roMonday" name="roMonday" value="true" />Monday<br />\r\n')
    optionsHtml.append('<input type="checkbox" id="roTuesday" name="roTuesday" value="true" />Tuesday<br />\r\n')
    optionsHtml.append('<input type="checkbox" id="roWednesday" name="roWednesday" value="true" />Wednesday<br />\r\n')
    optionsHtml.append('<input type="checkbox" id="roThursday" name="roThursday" value="true" /> Thursday<br />\r\n')
    optionsHtml.append('<input type="checkbox" id="roFriday" name="roFriday" value="true" /> Friday<br />\r\n')
    optionsHtml.append('<input type="checkbox" id="roSaturday" name="roSaturday" value="true" /> Saturday<br />\r\n')
    optionsHtml.append('<input type="checkbox" id="roSunday" name="roSunday" value="true" /> Sunday<br />\r\n')

    optionsHtml.append('</td>\r\n')
    optionsHtml.append('</tr>\r\n')
    optionsHtml.append('<tr>\r\n')
    optionsHtml.append('<td>Repeat options through:</td>\r\n')
    optionsHtml.append('<td><input type="text" name="persistOptionsThrough" id="persistOptionsThrough" size="9" /></td>\r\n')
    optionsHtml.append('</tr>\r\n')
    optionsHtml.append('<tr>\r\n')
    optionsHtml.append('<td colspan="2"><input type="submit" value="Edit Options" /></td>\r\n')
    optionsHtml.append('</tr>\r\n')
    optionsHtml.append('</table>\r\n')
    optionsHtml.append('</form>\r\n')
    finalHtml = "".join(optionsHtml)
    return HttpResponse(finalHtml)

@login_required   
def ajaxGetNotes(request):
    '''Responds to AJAX request to get all notes associated with a shift.'''
    notesToShow = Note.objects.filter(shifts__id=request.GET['shiftId'])
    html = []
    # Here we build a div for each note and return the HTML.
    for eachNote in notesToShow:
        noteHtml = []
        noteHtml.append("<div class='note'><div class='noteHeader'>")
        noteHtml.append(eachNote.title+" by "+eachNote.author.username+" at "+eachNote.date.strftime("%I:%M %p on %m/%d/%Y"))
        noteHtml.append("</div><div class='noteBody'>")
        noteHtml.append(eachNote.body)
        noteHtml.append("</div></div>")
        html.append("".join(noteHtml))
        
    finalHtml = "".join(html)
    return HttpResponse(finalHtml)
    
@login_required
def ajaxAddNote(request):
    '''Responds to an AJAX request to add a note to a shift.'''
    
    # If we want to assign the note to multil;e shifts over multiple days, and
    # they have specified that in the form, we do this.
    if 'startDate' in request.POST:
        try:
            x = request.POST['startDate'].split('/')
            startDate = datetime.date(int(x[2]), int(x[0]), int(x[1]))
        except:
            startDate = datetime.datetime.now()
    else:
        startDate = datetime.datetime.now()
        
    if 'endDate' in request.POST:
        try:
            y = request.POST['endDate'].split('/')
            endDate = datetime.date(int(y[2]), int(y[0]), int(y[1]))
        except:
            endDate = startDate
    else:
        endDate = startDate
    
    # Create the new note and give it some info.
    newNote = Note()
    newNote.title = request.POST['newNoteTitle']
    newNote.body = request.POST['newNoteBody']
    newNote.author = request.user
    newNote.date = datetime.datetime.now()
    newNote.save()
    
    # Get the days to repeat the note
    daysToReoccur = utility.getRepeatedDays(request)
    
    # primeShifts holds the shifts that were initially selected on one page.
    primeShifts = []
    for eachKey in request.POST:
        if eachKey.isdigit():
            primeShifts.append(MicroShift.objects.get(id=int(eachKey)))
    
    # Now, we take each prime shift, and get all the other shifts the note should
    # apply to within a date range.
    for eachShift in primeShifts:
        followingShifts = MicroShift.objects.filter(date__lte=endDate, date__gte=startDate, location=eachShift.location, startingTime=eachShift.startingTime, endingTime=eachShift.endingTime)
        for eachShiftA in followingShifts:
            # Assuming each shift we found falls on a reocurring day, apply the note
            if eachShiftA.date.weekday() in daysToReoccur or eachShiftA.date == eachShift.date:
                newNote.shifts.add(eachShiftA)
                newNote.save()
            else:
                pass
    return HttpResponse("ok")

@login_required
def ajaxEditShiftOptions(request):
    '''
    Handles an AJAX request to edit shift options.
    '''
    if 'startDate' in request.POST:
        try:
            x = request.POST['startDate'].split('/')
            startDate = datetime.date(int(x[2]), int(x[0]), int(x[1]))
        except:
            startDate = datetime.datetime.now()
    else:
        startDate = datetime.datetime.now()
        
    if 'endDate' in request.POST:
        try:
            y = request.POST['endDate'].split('/')
            endDate = datetime.date(int(y[2]), int(y[0]), int(y[1]))
        except:
            endDate = startDate
    else:
        endDate = startDate


    daysToReoccur = utility.getRepeatedDays(request)
    if request.POST['showOnNews'] == "true":
        showOnNews = True
    else:
        showOnNews = False
    # primeShifts holds the shifts that were initially selected on one page.
    primeShifts = []
    for eachKey in request.POST:
        if eachKey.isdigit():
            primeShifts.append(MicroShift.objects.get(id=int(eachKey)))
    
    # Now, we take each prime shift, and get all the other shifts the note should
    # apply to within a date range.
    for eachShift in primeShifts:
        followingShifts = MicroShift.objects.filter(date__lte=endDate, date__gte=startDate, location=eachShift.location, startingTime=eachShift.startingTime, endingTime=eachShift.endingTime)
        for eachShiftA in followingShifts:
            # Assuming each shift we found falls on a reocurring day, apply the note
            if eachShiftA.date.weekday() in daysToReoccur or eachShiftA.date == eachShift.date:
                eachShiftA.showOnNews = showOnNews
                eachShiftA.save() 
            else:
                pass
    return HttpResponse("ok")

@login_required
def ajaxUpdateProfile(request):
    '''
    Handles AJAX request to update a user's profile. If a supervisor has
    specified a worker on the page, we get targetWorker and use that. if not, we
    default to the user requesting the page.
    '''
    try:
        # If targetWorker is in the request, we know it is a supervisor editing
        # someone else's profile.
        if 'targetWorker' in request.POST:
            if request.POST['actionToTake'] == 'edit':
                user = User.objects.get(username=request.POST['targetWorker'])
                profile = user.get_profile()
                user.first_name = request.POST['firstName']
                user.last_name = request.POST['lastName']
                user.email = request.POST['email']
                profile.color = request.POST['background']
                profile.textColor = request.POST['foreground']
                user.save()
                profile.save()
            elif request.POST['actionToTake'] == 'delete':
                user = User.objects.get(username=request.POST['targetWorker'])
                user.is_active = False
            else:
                # TODO: Error checking here
                pass
            
        else:
            profile = request.user.get_profile()
            user = request.user
            user.first_name = request.POST['firstName']
            user.last_name = request.POST['lastName']
            user.email = request.POST['email']
            profile.color = request.POST['background']
            profile.textColor = request.POST['foreground']
            user.save()
            profile.save()
    except:
        pass

    
    return HttpResponse("ok")

@login_required
def ajaxGetAvailableWorkers(request):
    '''Responds to an AJAX request to get available workers for a shift or group of shifts.'''
    
    htmlRows = []
    # If the user selected multiple shifts to check, we'll do this. 
    if 'multiSelect' in request.GET:
        # List of shift objects to check
        shifts = []
        for eachKey in request.GET:
            if eachKey.isdigit():
                shifts.append(MicroShift.objects.get(id=eachKey))
        workers = []
        for eachWorker in UserProfile.objects.all():
            workers.append(eachWorker)
        tableRows = []
        daysMap = {0:'monday', 1:'tuesday', 2:'wednesday', 3:'thursday', 4:'friday', 5:'saturday', 6:'sunday'}
        
        # Here we check the time slot of the shift against the corresponding availability slot of each worker
        for eachWorker in UserProfile.objects.all():
            for shift in shifts:
                attrTimeStart = shift.startingTime.strftime("%H%M")
                attrTimeEnd = shift.endingTime.strftime("%H%M")
                day = daysMap[shift.date.weekday()]
                
                # TODO: Put this in its own function. It creates an attribute string to
                # check using getattr. i.e., attrstring = monday800to830 or whatever
                if attrTimeStart != "0000" and attrTimeStart[0] == "0":
                    attrTimeStart = attrTimeStart[1:]
                if attrTimeEnd != "0000" and attrTimeEnd[0] == "0":
                    attrTimeEnd = attrTimeEnd[1:]
                attrString = day+attrTimeStart+'to'+attrTimeEnd
                
                # If they are not available, we remove them from the available list
                if getattr(eachWorker, attrString) == "Unavailable":
                    # We check this as we may have already removed them if they were
                    # unavailable for a previous shift we checked
                    if eachWorker in workers:
                        workers.remove(eachWorker)
        
        # Now just build the table to send back
        htmlRows.append("<table id='availableWorkerTable'>")
        for eachWorker in workers:
            if eachWorker.user.is_active:
                htmlRows.append("<tr><td class='workerGenericAvailable'>"+eachWorker.user.username+"</td>")
                htmlRows.append("</td></tr>")
        htmlRows.append("</table>")
        html = "".join(htmlRows)
        return HttpResponse(html)

    else:
        # If they user is just checking one shift...
        shift = MicroShift.objects.get(id=request.GET['shiftId'])
        tableRows = []
        attrTimeStart = shift.startingTime.strftime("%H%M")
        attrTimeEnd = shift.endingTime.strftime("%H%M")
        daysMap = {0:'monday', 1:'tuesday', 2:'wednesday', 3:'thursday', 4:'friday', 5:'saturday', 6:'sunday'}
        day = daysMap[shift.date.weekday()]
        if attrTimeStart != "0000" and attrTimeStart[0] == "0":
            attrTimeStart = attrTimeStart[1:]
        if attrTimeEnd != "0000" and attrTimeEnd[0] == "0":
            attrTimeEnd = attrTimeEnd[1:]
        attrString = day+attrTimeStart+'to'+attrTimeEnd
        availableWorkers = []
    
        htmlRows = []
        htmlRows.append("<table id='availableWorkerTable'>")
        
        # Since they are just checking one, we can color code each one based on their availability preferences, as well as
        # create a form to allow for one-click assigning
        for eachWorker in UserProfile.objects.all():
            
            if getattr(eachWorker, attrString) == "Preferred":
                htmlRows.append("<form class='reconcileForm' action='/ajaxAssignWorkerToShift/' method='post'>")
                htmlRows.append("<tr><td class='workerAvailablePreferred'>"+eachWorker.user.username+"</td>")
                htmlRows.append("<td class='reconcileAssignButton'><input type='submit' value='Assign' />")
                htmlRows.append("<input type='hidden' name='shiftId' value='"+str(shift.id)+"' />")
                htmlRows.append("<input type='hidden' name='targetWorker' value='"+eachWorker.user.username+"' />")
                htmlRows.append("</td></form></tr>")
            elif getattr(eachWorker, attrString) == "Not Preferred":
                htmlRows.append("<form class='reconcileForm' action='/ajaxAssignWorkerToShift/' method='post'>")
                htmlRows.append("<tr><td class='workerAvailableNotPreferred'>"+eachWorker.user.username+"</td>")
                htmlRows.append("<td class='reconcileAssignButton'><input type='submit' value='Assign' />")
                htmlRows.append("<input type='hidden' name='shiftId' value='"+str(shift.id)+"' />")
                htmlRows.append("<input type='hidden' name='targetWorker' value='"+eachWorker.user.username+"' />")
                htmlRows.append("</td></form></tr>")
            elif getattr(eachWorker, attrString) == "Available":
                htmlRows.append("<form class='reconcileForm' action='/ajaxAssignWorkerToShift/' method='post'>")
                htmlRows.append("<tr><td class='workerAvailableAvailable'>"+eachWorker.user.username+"</td>")
                htmlRows.append("<td class='reconcileAssignButton'><input type='submit' value='Assign' />")
                htmlRows.append("<input type='hidden' name='shiftId' value='"+str(shift.id)+"' />")
                htmlRows.append("<input type='hidden' name='targetWorker' value='"+eachWorker.user.username+"' />")
                htmlRows.append("</td></form></tr>")
    
        htmlRows.append("</table>")
        html = "".join(htmlRows)
        return HttpResponse(html)

@login_required
def ajaxAssignWorkerToShift(request):
    '''
    Takes an AJAX request to assign a user to a shift.
    Accepts a shiftId and targetWorker (the worker name) from the
    web page.
    '''
    
    shift = MicroShift.objects.get(id=request.POST['shiftId'])
    user = User.objects.get(username=request.POST['targetWorker'])
    shift.worker = user
    shift.save()
    return HttpResponse("ok")
    
@login_required
def ajaxEditLocations(request):
    '''Renders and displays the edit locations page.'''
    
    # We include the supervisors so we can pass it to the template
    supervisorSet = UserProfile.objects.filter(isSupervisor=True)
    supervisors = []
    for eachS in supervisorSet:
        supervisors.append(eachS.user.username)
    locations = Location.objects.all()
    t = loader.get_template("ajax_edit_locations.html")
    html = t.render(RequestContext(request, {'supervisors':supervisors, 'locations':locations}))
    return HttpResponse(html)
    
@login_required
def ajaxModifyLocation(request):
    '''Accepts an AJAX request to edit a location. Takes a locationId.'''
    
    location = Location.objects.get(id=request.POST['locationId'])

    if 'deleteShift' in request.POST:
        # TODO: Do we want to do something to preserve shifts associated with it?
        location.delete()
    else:
        # Replace the shift info with all the new info we received
        newSupervisor = User.objects.get(username=request.POST['newSupervisor'])
        location.locationName = request.POST['newLocationName']
        location.supervisor = newSupervisor
        location.save()

    # Now we build the new table containing the altered location info to send back
    # TODO: Break this into a separate function for AJAX GET or something?
    locations = Location.objects.all()
    supervisors = UserProfile.objects.filter(isSupervisor=True)
    htmlRows = []
    for eachLocation in locations:
        htmlRows.append('<table class="locationToEdit"><tr>')
        htmlRows.append('<form class="editLocationForm" action="/ajaxModifyLocation/" method="post">')
        htmlRows.append('<td class="locationToEditName"><input id="newLocationName" name="newLocationName" size="40" type="text" value="'+eachLocation.locationName+'" /></td>')
        htmlRows.append('<td class="locationToEditSupervisor">')
        htmlRows.append('<input type="hidden" name="locationId" id="locationId" value="'+str(eachLocation.id)+'" />')
        htmlRows.append('<select id="newSupervisor" name="newSupervisor">')
        for eachSupervisor in supervisors:
            if eachLocation.supervisor.username == eachSupervisor.user.username:
                htmlRows.append('<option value="'+eachSupervisor.user.username+'" selected="selected">'+eachSupervisor.user.username+'</option>')
            else:
                htmlRows.append('<option value="'+eachSupervisor.user.username+'">'+eachSupervisor.user.username+'</option>')
        htmlRows.append('</select></td>')
        htmlRows.append('<td><input type="checkbox" id="toDelete" name="toDelete" value="toDelete">Delete?</input></td>')
        htmlRows.append('<td class="locationToEditButton">')
        htmlRows.append('<input type="submit" value="Edit" /></td></form></tr></table>')
    return HttpResponse("".join(htmlRows))

@login_required
def ajaxEditShifts(request):
    '''Renders and displays the add/edit shift form.'''
    locations = Location.objects.all()
    t = loader.get_template("ajax_edit_shifts.html")
    html = t.render(RequestContext(request, {'locations':locations}))
    return HttpResponse(html)
    
def ajaxEditShift(request):
    '''Responds to an AJAX request to edit a shift.'''
    
    # TODO: Put this in a module somewhere...
    daysToReoccur = []
    dayNameToNumberMap = {}
    dayNameToNumberMap[0] = 'Monday'
    dayNameToNumberMap[1] = 'Tuesday'
    dayNameToNumberMap[2] = 'Wednesday'
    dayNameToNumberMap[3] = 'Thursday'
    dayNameToNumberMap[4] = 'Friday'
    dayNameToNumberMap[5] = 'Saturday'
    dayNameToNumberMap[6] = 'Sunday'
    ####
    
    rMonday = False
    rTuesday = False
    rWednesday = False
    rThursday = False
    rFriday = False
    rSaturday = False
    rSunday = False
    
    # If we want to add a shift...
    if request.POST['actionToTake'] == 'add':
        
        # We get the start date, end date, etc, all the info we need
        startDate = request.POST['startDate']
        description = request.POST['shiftDescription']
        endDate = request.POST['endDate']
        startTime = utility.parseTime(utility.twelveToTwentyFour(request.POST["startHour"], request.POST["startMinute"], request.POST["startAmPm"]))
        endTime = utility.parseTime(utility.twelveToTwentyFour(request.POST["endHour"], request.POST["endMinute"], request.POST["endAmPm"]))
        location = Location.objects.get(locationName=request.POST['location'])

        # This is a remnant when I was toying with the idea of not breaking apart shifts unless needed
        # but that proved too difficult and of little benefit.
        breakApart = True
        
        # If it repeats every day, just add all the days
        if 'rAll' in request.POST:
            daysToReoccur.append(0)
            daysToReoccur.append(1)
            daysToReoccur.append(2)
            daysToReoccur.append(3)
            daysToReoccur.append(4)
            daysToReoccur.append(5)
            daysToReoccur.append(6)
            rMonday = True
            rTuesday = True
            rWednesday = True
            rThursday = True
            rFriday = True
            rSaturday = True
            rSunday = True
            
        else:
            # Check for each day reoccurence
            # TODO: It's own function!!
            if 'rMonday' in request.POST and request.POST['rMonday'] == "true":
                daysToReoccur.append(0)
                rMonday = True
            if 'rTuesday' in request.POST and request.POST['rTuesday'] == "true":
                daysToReoccur.append(1)
                rTuesday = True
            if 'rWednesday' in request.POST and request.POST['rWednesday'] == "true":
                daysToReoccur.append(2)
                rWednesday = True
            if 'rThursday' in request.POST and request.POST['rThursday'] == "true":
                daysToReoccur.append(3)
                rThursday = True
            if 'rFriday' in request.POST and request.POST['rFriday'] == "true":
                daysToReoccur.append(4)
                rFriday = True
            if 'rSaturday' in request.POST and request.POST['rSaturday'] == "true":
                daysToReoccur.append(5)
                rSaturday = True
            if 'rSunday' in request.POST and request.POST['rSunday'] == "true":
                daysToReoccur.append(6)
                rSunday = True
            
        x = startDate.split('/')
        y = endDate.split('/')
        startDateTime = datetime.date(int(x[2]), int(x[0]), int(x[1]))
        endDateTime = datetime.date(int(y[2]), int(y[0]), int(y[1]))
        oneDay = datetime.timedelta(days=1)
        thirtyMinutes = datetime.timedelta(minutes=30)
        # We create an initial OverShift with the start time and end time
        overShift = OverShift.objects.create(description=description, \
                                             location = location, \
                                             startingTime = startTime, \
                                             endingTime = endTime, \
                                             recurmonday = rMonday, \
                                             recurtuesday = rTuesday, \
                                             recurwednesday = rWednesday, \
                                             recurthursday = rThursday, \
                                             recurfriday = rFriday, \
                                             recursaturday = rSaturday, \
                                             recursunday = rSunday, \
                                             startingDate = startDateTime, \
                                             endingDate = endDateTime)
        overShift.save()
    
        if breakApart:
            # Now we'll create a series of microshifts that fill all the 30 minute blocks
            # within that overshift's begin/end range
            
            # If we need to do the days to reoccur processing...
            if daysToReoccur:
                while startDateTime <= endDateTime:
                    if startDateTime.weekday() in daysToReoccur:
                        beginTime = datetime.time(startTime.hour, startTime.minute)
                        goalTime = datetime.time(endTime.hour, endTime.minute)
                        
                        while beginTime != goalTime:
                            
                            # If a shift already exists at that tiem at that location on that date,
                            # we don't want to create a new one
                            testShiftExists = MicroShift.objects.filter(date=startDateTime, \
                                                                startingTime = beginTime, \
                                                                location=location)
                            if not testShiftExists:
                                newMicroShift = MicroShift.objects.create(date=startDateTime, \
                                                                      startingTime = beginTime, \
                                                                      endingTime = utility.addMins(beginTime, 30), \
                                                                      location = location, \
                                                                      overShift = overShift)
                                newMicroShift.save()
                            else:
                                pass
                            # Add 30 min to the tracking time
                            beginTime = utility.addMins(beginTime, 30)                            
                    startDateTime += oneDay
                    
            # Same as above, but with no reoccurence checking
            else:
                beginTime = datetime.time(startTime.hour, startTime.minute)
                goalTime = datetime.time(endTime.hour, endTime.minute)
                while beginTime != goalTime:
                    testShiftExists = MicroShift.objects.filter(date=startDateTime, \
                                                                startingTime = beginTime, \
                                                                location=location)

                    if not testShiftExists:
                        newMicroShift = MicroShift.objects.create(date=startDateTime, \
                                                              startingTime = beginTime, \
                                                              endingTime = utility.addMins(beginTime, 30), \
                                                              location = location, \
                                                              overShift = overShift)
                        newMicroShift.save()
                    else:
                        pass
                    beginTime = utility.addMins(beginTime, 30)
                    
        return json_response({"resultString":'ok'})
        
    elif request.POST['actionToTake'] == 'remove':
        if 'rAll' in request.POST:
            daysToReoccur.append(0)
            daysToReoccur.append(1)
            daysToReoccur.append(2)
            daysToReoccur.append(3)
            daysToReoccur.append(4)
            daysToReoccur.append(5)
            daysToReoccur.append(6)
            rMonday = True
            rTuesday = True
            rWednesday = True
            rThursday = True
            rFriday = True
            rSaturday = True
            rSunday = True
            
        else:
            if 'rMonday' in request.POST and request.POST['rMonday'] == "true":
                daysToReoccur.append(0)
                rMonday = True
            if 'rTuesday' in request.POST and request.POST['rTuesday'] == "true":
                daysToReoccur.append(1)
                rTuesday = True
            if 'rWednesday' in request.POST and request.POST['rWednesday'] == "true":
                daysToReoccur.append(2)
                rWednesday = True
            if 'rThursday' in request.POST and request.POST['rThursday'] == "true":
                daysToReoccur.append(3)
                rThursday = True
            if 'rFriday' in request.POST and request.POST['rFriday'] == "true":
                daysToReoccur.append(4)
                rFriday = True
            if 'rSaturday' in request.POST and request.POST['rSaturday'] == "true":
                daysToReoccur.append(5)
                rSaturday = True
            if 'rSunday' in request.POST and request.POST['rSunday'] == "true":
                daysToReoccur.append(6)
                rSunday = True
                
        if request.user.get_profile().isSupervisor:
            st = utility.twelveToTwentyFour(request.POST["startHour"], request.POST["startMinute"], request.POST["startAmPm"])
            et = utility.twelveToTwentyFour(request.POST["endHour"], request.POST["endMinute"], request.POST["endAmPm"])
        if request.POST['endDate'] != "":
            ed = request.POST['endDate'].split("/")
        else:
            ed = None
        errorResult = utility.processShiftDelete(st, et, request.POST['startDate'], request.POST['location'], ed, request.user, daysToReoccur)
        if errorResult == False:
            return json_response({'resultString':'ok'})
        else:
            return json_response({'resultString':'error'})
    else:
        return json_response({"resultString":'Weird error.'})

@login_required
def ajaxViewChangeShiftRequests(request):
    '''Renders and displays all the pending change shift requests.'''
    t = loader.get_template("ajax_view_shift_change_requests.html")
    # TODO: Check for supervisor in template
    html = t.render(RequestContext(request, {}))
    return HttpResponse(html)
    
@login_required
def ajaxGetChangeShiftRequests(request):
    shiftChangeRequests = ChangeRequest.objects.filter(status="pending").order_by("dateRequested")
    htmlRows = []
    for eachRequest in shiftChangeRequests:
        if eachRequest.requestType == 'claim':
            htmlRows.append("<h3>")
            htmlRows.append("<a href='#'>Claim request by "+eachRequest.requestedBy.username+" on "+eachRequest.dateRequested.strftime("%m/%d/%Y")+" at "+eachRequest.shiftsToChange.all()[0].location.locationName+" </a>")
            htmlRows.append("</h3>")
        elif eachRequest.requestType == 'drop':
            htmlRows.append("<h3><a href='#'>Drop request by "+eachRequest.requestedBy.username+" on "+eachRequest.dateRequested.strftime("%m/%d/%Y")+" at "+eachRequest.shiftsToChange.all()[0].location.locationName+"</a></h3>")
        
        # TODO: Check for this and throw an error...should always be claim or drop
        else:
            htmlRows.append("<h3><a href='#'>UnknownType request by "+eachRequest.requestedBy.username+" on "+eachRequest.dateRequested.strftime("%m/%d/%Y")+"</a></h3>")
        
        htmlRows.append("<div>")
        htmlRows.append("<form name='bulkChangeRequest' class='changeRequestForm' method='post' action='/ajaxProcessChangeRequest/'>")
        htmlRows.append("<button type='button' class='approveAll' name='approveAll'>Approve All</button>")
        htmlRows.append("<button type='button' class='denyAll' name='denyAll'>Deny All</button>")
        htmlRows.append("<input type='hidden' name='changeRequestId' value='"+str(eachRequest.id)+"' />")
        htmlRows.append("</form>")
        
        htmlRows.append("<div class='changeReasonLabel'>Reason</div>")
        htmlRows.append("<div class='changeReason'>"+eachRequest.reason+"</div>")
        for eachAttachedShift in eachRequest.shiftsToChange.all():
            htmlRows.append("<form name='individualChangeRequest' class='changeRequestForm' method='post' action='/ajaxProcessChangeRequest/'>")
            htmlRows.append("<div class='subShift'>"+eachAttachedShift.startingTime.strftime('%I:%M %p')+"-"+eachAttachedShift.endingTime.strftime('%I:%M %p')+' on '+eachAttachedShift.date.strftime('%m/%d/%Y'))
            htmlRows.append("<input type='hidden' name='shiftId' value='"+str(eachAttachedShift.id)+"' />")
            htmlRows.append("<input type='hidden' name='changeRequestId' value='"+str(eachRequest.id)+"' />")
            htmlRows.append("<button class='approveIndividual' type='button' name='approveChange'>Approve</button>")
            htmlRows.append("<button type='button' class='denyIndividual' name='denyChange'>Deny</button></form>")     
            htmlRows.append("</div>")
        
        htmlRows.append("</div>")
        
    return HttpResponse("".join(htmlRows))
         
@login_required
def ajaxProcessChangeRequest(request):
    if request.POST['requestType'] == 'approveAll':
        changeRequest = ChangeRequest.objects.get(id=request.POST['changeRequestId'])
        for eachShift in changeRequest.shiftsToChange.all():
            if changeRequest.requestType == 'claim':
                eachShift.worker = changeRequest.requestedBy
            elif changeRequest.requestType == 'drop':
                eachShift.worker = None
            eachShift.save()
        changeRequest.dateActedOn = datetime.datetime.now()
        changeRequest.result = "approved"
        changeRequest.status = "done"
        changeRequest.actedOnBy = request.user
        changeRequest.save()
        
    elif request.POST['requestType'] == 'denyAll':
        changeRequest = ChangeRequest.objects.get(id=request.POST['changeRequestId'])
        changeRequest.dateActedOn = datetime.datetime.now()
        changeRequest.result = "denied"
        changeRequest.status = "done"
        changeRequest.actedOnBy = request.user
        for eachShift in changeRequest.shiftsToChange.all():
            eachShift.showOnNews = True
            eachShift.save()
        changeRequest.save()
        
    elif request.POST['requestType'] == 'approveIndividual':
        shift = MicroShift.objects.get(id=request.POST['shiftId'])
        parentChangeRequest = ChangeRequest.objects.get(id=request.POST['changeRequestId'])
        newIndividualChangeRequest = IndividualChangeRequest()
        newIndividualChangeRequest.parentChangeRequest = parentChangeRequest
        newIndividualChangeRequest.result = "done"
        parentChangeRequest.shiftsToChange.remove(shift)
        if not parentChangeRequest.shiftsToChange.all():
            parentChangeRequest.status = 'done'
        if parentChangeRequest.requestType == 'claim':
            shift.worker = parentChangeRequest.requestedBy
        elif parentChangeRequest.requestType == 'drop':
            shift.worker = None
        shift.save()
        parentChangeRequest.save()
        
    elif request.POST['requestType'] == 'denyIndividual':
        shift = MicroShift.objects.get(id=request.POST['shiftId'])
        shift.showOnNews = True
        parentChangeRequest = ChangeRequest.objects.get(id=request.POST['changeRequestId'])
        newIndividualChangeRequest = IndividualChangeRequest()
        newIndividualChangeRequest.parentChangeRequest = parentChangeRequest
        newIndividualChangeRequest.result = "done"
        parentChangeRequest.shiftsToChange.remove(shift)
        if not parentChangeRequest.shiftsToChange.all():
            parentChangeRequest.status = 'done'
        shift.save()
        parentChangeRequest.save()

    else:
        return HttpResponse("Invalid requestType. Please try again.")
    return HttpResponse("OK")
        
def ajaxRegister(request):
    if request.user.is_authenticated():
        return HttpResponse("<h1>You are already registered!</h3>")
    t = loader.get_template("ajax_register.html")
    html = t.render(RequestContext(request, {}))
    return HttpResponse(html)
    
def ajaxRegisterNew(request):

    requestedUserName = request.POST['userName']
    requestedPassword = request.POST['password']
    confirmPassword = request.POST['confirmPassword']
    if requestedPassword != confirmPassword:
        return json_response({result:"error", errorString:"Your passwords did not match."})
    firstName = request.POST['firstName']
    lastName = request.POST['lastName']
    email = request.POST['email']
    vUser = User.objects.filter(username=requestedUserName)
    if vUser:
        return json_response({result:"error", errorString:"That user already exists."})
    else:
        new_user = User.objects.create_user(requestedUserName, email, requestedPassword)
        new_user.is_active = True
        new_user.first_name = firstName
        new_user.last_name = lastName
        new_user.save()
        user = authenticate(username=requestedUserName, password=requestedPassword)
        auth_login(request, user)
        return HttpResponseRedirect('/ajaxHome/')
        
def ajaxLogout(request):
    auth_logout(request)
    return HttpResponseRedirect("/ajaxLogin/")
    
def outputCalendar(request):
    try:
        thingsToShow = utility.decode_data(request.GET['hash'], request.GET['enc'])
        listOfThingsToShow = thingsToShow.split("&")
        
        locationsToShow = []
        usersToShow = []
        for eachThing in listOfThingsToShow:
            
            locationTest = Location.objects.filter(locationName=eachThing)
            if locationTest:
                locationsToShow.append(eachThing)
            
            userTest = User.objects.filter(username=eachThing)
            if userTest:
                usersToShow.append(eachThing)
            
        locationsToInclude = []
        for eachKey in request.GET:
            for eachLocation in Location.objects.all():
                if eachKey == eachLocation.locationName:
                    locationsToInclude.append(eachKey)
                else:
                    pass
        
        events = MicroShift.objects.filter(location__locationName__in=locationsToShow, worker__username__in=usersToShow)
        cal = icalendar.Calendar()
        site = Site.objects.get_current()
        cal.add('prodid', '-//%s Schedule Calendar//%s//' % (site.name, site.domain))
        cal.add('version', '2.0')
        
        site_token = site.domain.split(".")
        site_token.reverse()
        site_token = ".".join(site_token)
        
        tzPST = timezones.USTimeZone(-7, "Pacific", "PST", "PDT")
        for eachEvent in events:
            ical_event = icalendar.Event()
            try:
                ical_event.add('summary', eachEvent.worker.username+"'s shift at "+eachEvent.location.locationName)
            except AttributeError:
                ical_event.add('summary', 'Unassigned shift at'+eachEvent.location.locationName)
            evYear = eachEvent.date.year
            evMonth = eachEvent.date.month
            evDay = eachEvent.date.day
            evStartHour = eachEvent.startingTime.hour
            evStartMinute = eachEvent.startingTime.minute
            evEndHour = eachEvent.endingTime.hour
            evEndMinute = eachEvent.endingTime.minute
            evStartingTime = datetime.datetime(evYear, evMonth, evDay, evStartHour, evStartMinute, tzinfo=tzPST)
            evEndingTime = datetime.datetime(evYear, evMonth, evDay, evEndHour, evEndMinute, tzinfo=tzPST)
            ical_event.add('dtstart', evStartingTime)
            ical_event.add('dtend', evEndingTime)
            ical_event.add('dtstamp', datetime.datetime.now())
            ical_event['uid'] = '%d.event.events.%s' % (eachEvent.id, site_token)
            ical_event.add('priority', 5)
            cal.add_component(ical_event)
        
        response = HttpResponse(cal.as_string(), mimetype="text/calendar")
        response['Content-Disposition'] = "attachment; filename=savuschedule.ics"
        return response
    except:
        return HttpResponse("Invalid URL!")
        
def getIcalString(request):
    hash, enc = utility.encode_data(request.GET['icalStringA'])
    encodedString = 'hash='+hash+"&enc="+enc
    return HttpResponse(encodedString)
