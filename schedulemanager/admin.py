from schedulemanager.models import Location, OverShift, MicroShift, UserProfile, NewsPost, Note, ChangeRequest, AppPreferences
from schedulemanager.models import IndividualChangeRequest
from django.contrib import admin

admin.site.register(Location)
admin.site.register(OverShift)
admin.site.register(MicroShift)
admin.site.register(UserProfile)
admin.site.register(NewsPost)
admin.site.register(Note)
admin.site.register(ChangeRequest)
admin.site.register(AppPreferences)
admin.site.register(IndividualChangeRequest)