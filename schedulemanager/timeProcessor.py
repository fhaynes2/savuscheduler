### This file is part of the Savu Scheduler application
### Copyright 2011 Fletcher Haynes

### Licensed under the Apache License, Version 2.0 (the "License");
### you may not use this file except in compliance with the License.
### You may obtain a copy of the License at
### 
###     http://www.apache.org/licenses/LICENSE-2.0
### 
### Unless required by applicable law or agreed to in writing, software
### distributed under the License is distributed on an "AS IS" BASIS,
### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
### See the License for the specific language governing permissions and
### limitations under the License.
import datetime
def convertTime(time):
    time = time.lstrip('0')
    newTime = ''
    if len(time) == 3:
     
        if time[1:] == '30':
            newTime = time[0]+'50'
        else:
            return time
    elif len(time) == 4:
        if time[2:] == '30':

            newTime = time[:2]+'50'
        else:
            print time
            return time
        
    else:
        pass
    return newTime

def scrubTime(timeString):
    if timeString[0] == "0":
        if timeString[1] == "0":
            return timeString
        else:
            timeString = timeString[1:4]
            return timeString
    
    else:
        return timeString

def processBlock(profile, startTime, endTime, day, preference):
    startTimeObj = datetime.datetime(1950,1,1,int(startTime[0:2]), int(startTime[2:4]))
    thirtyMinutes = datetime.timedelta(minutes=30)
    endTimeObj = datetime.datetime(1950,1,1,int(endTime[0:2]), int(endTime[2:4]))
    endTimeObj += thirtyMinutes
    startShiftTime = startTimeObj
    endShiftTime = startShiftTime + thirtyMinutes
    while endShiftTime.time() != endTimeObj.time():
        stToSet = scrubTime(startShiftTime.strftime("%H%M"))
        if endShiftTime.strftime("%H%M") == "0000":
            etToSet = scrubTime("2359")
        else:
            etToSet = scrubTime(endShiftTime.strftime("%H%M"))
        #print stToSet, etToSet
        setattr(profile, day.lower()+stToSet+'to'+etToSet, preference)
        startShiftTime += thirtyMinutes
        endShiftTime += thirtyMinutes

        profile.save()

def calculateCurrentHours(profile, shifts):
    curTotal = datetime.timedelta()
    for eachShift in shifts:
        sdt = datetime.datetime(eachShift.date.year, eachShift.date.month, eachShift.date.day, eachShift.startingTime.hour, eachShift.startingTime.minute, 0)
        edt = datetime.datetime(eachShift.date.year, eachShift.date.month, eachShift.date.day, eachShift.endingTime.hour, eachShift.endingTime.minute, 0)
        #curTotal+=(edt-sdt)
    #    print edt-sdt
    #print curTotal
        
        
    
