### This file is part of the Savu Scheduler application
### Copyright 2011 Fletcher Haynes

### Licensed under the Apache License, Version 2.0 (the "License");
### you may not use this file except in compliance with the License.
### You may obtain a copy of the License at
### 
###     http://www.apache.org/licenses/LICENSE-2.0
### 
### Unless required by applicable law or agreed to in writing, software
### distributed under the License is distributed on an "AS IS" BASIS,
### WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
### See the License for the specific language governing permissions and
### limitations under the License.
import datetime
from schedulemanager.models import Location, MicroShift, ChangeRequest
import hashlib, zlib, urllib
import cPickle as pickle
oneDay = datetime.timedelta(days=1)


dayNameToNumberMap = {}
dayNameToNumberMap['Monday'] = 0
dayNameToNumberMap['Tuesday'] = 1
dayNameToNumberMap['Wednesday'] = 2
dayNameToNumberMap['Thursday'] = 3
dayNameToNumberMap['Friday'] = 4
dayNameToNumberMap['Saturday'] = 5
dayNameToNumberMap['Sunday'] = 6

def getLocationNames():
    locationNames = []
    locations = Location.objects.all()
    for eachLocation in locations:
        locationNames.append(eachLocation)
    return locationNames

def createFirstRow(startDate, endDate, numLocs):
    firstRow = []

    # We append this so that the dates don't start directly over the column that shows the times

    curDate = startDate
    while curDate <= endDate:
	dateString = curDate.strftime('%m/%d/%Y (%A)')
        firstRow.append('<td>'+dateString+'</td>')
        # We need to create a blank td for every location so they stay synced, so we append a "" placeholder.
        for x in range(0, numLocs):
            firstRow.append("<td></td>")
        curDate += oneDay

    return firstRow
        
def createSecondRow(startDate, endDate, locations):
    curDate = startDate
    # Let's create the second row, which will contain the location name
    secondRow = []
    secondRow.append("<td></td>")
    while curDate <= endDate:
        for eachLocation in locations:
            secondRow.append('<td>'+eachLocation.locationName+'</td>')
        if curDate != endDate:
            secondRow.append("<td></td>")
        curDate += oneDay
    return secondRow

def parseTime(timeString):
    if len(timeString) == 4:
        time = datetime.datetime(1901,1,1,int(timeString[:2]), int(timeString[2:]))
        return time
    else:
        time = datetime.datetime(1901,1,1,int(timeString[0]), int(timeString[1:3]))
        return time
    
def twelveToTwentyFour(hour, minute, ampm):
    if ampm == 'AM':
        if hour == "12":
            return '00'+str(minute)
        if len(hour) == 1:
            hour = '0'+str(hour)
            return hour+minute
        return hour+minute
    else:
        if hour == "12":
            return "12"+str(minute)
        else:
            hour = int(hour)+12
            return str(hour)+str(minute)
            
def twentyFourToTwelve(timeString):
    if len(timeString) == 3:
        timeObj = datetime.datetime(1901,1,1,int(timeString[0]), int(timeString[1:3]))
    else:
        timeObj = datetime.datetime(1901,1,1,int(timeString[:2]), int(timeString[2:4]))
    return timeObj.strftime("%I:%M%p")
    
def processShiftClaim(shiftId, user):
    shiftToChange = MicroShift.objects.get(id=shiftId)
    if shiftToChange.worker == None:
        shiftToChange.worker = user
        shiftToChange.save()
        return "ok"
    else:
        return "alreadyassigned"

def generateMultiDayShiftClaimRequest(shiftsToAddToRequest, user, endDate, repeatDays, reason=""):
    daysMap = {0:'monday', 1:'tuesday', 2:'wednesday', 3:'thursday', 4:'friday', 5:'saturday', 6:'sunday'}
    targetMonth, targetDay, targetYear = endDate.split("/")
    targetEndDate = datetime.date(int(targetYear), int(targetMonth), int(targetDay))
    newShiftChangeRequest = ChangeRequest()

    newShiftChangeRequest.dateRequested = datetime.datetime.now()
    newShiftChangeRequest.requestedBy = user
    newShiftChangeRequest.requestType = 'claim'
    newShiftChangeRequest.reason = reason
    newShiftChangeRequest.startDate = MicroShift.objects.get(id=shiftsToAddToRequest[0]).date
    newShiftChangeRequest.endDate = targetEndDate
    newShiftChangeRequest.status = "pending"
    newShiftChangeRequest.save()

    
    shiftsToPutInRequest = []
    
    for eachShift in shiftsToAddToRequest:
        startingShift = MicroShift.objects.get(id=eachShift)
        shiftsToCheckForRepeat = MicroShift.objects.filter(date__gte=startingShift.date, date__lte=targetEndDate, location=startingShift.location, \
                                                       startingTime=startingShift.startingTime, endingTime=startingShift.endingTime)
        for eachRepeatingShift in shiftsToCheckForRepeat:
            if daysMap[eachRepeatingShift.date.weekday()] in repeatDays:
                if eachRepeatingShift.worker == None:
                    shiftsToPutInRequest.append(eachRepeatingShift)
    
    for eachShift in shiftsToPutInRequest:
        newShiftChangeRequest.shiftsToChange.add(eachShift)
        eachShift.showOnNews = False
        eachShift.save() 
    newShiftChangeRequest.save()

def generateSingleDayShiftClaimRequest(shiftsToAddToRequest, user, reason=""):
    newShiftChangeRequest = ChangeRequest()
    newShiftChangeRequest.dateRequested = datetime.datetime.now()
    newShiftChangeRequest.requestedBy = user
    newShiftChangeRequest.requestType = 'claim'
    newShiftChangeRequest.reason = reason
    newShiftChangeRequest.startDate = MicroShift.objects.get(id=shiftsToAddToRequest[0]).date
    newShiftChangeRequest.endDate = MicroShift.objects.get(id=shiftsToAddToRequest[0]).date 
    newShiftChangeRequest.status = "pending"
    newShiftChangeRequest.save()

    for eachShift in shiftsToAddToRequest:
        targetShift = MicroShift.objects.get(id=eachShift)
        targetShift.showOnNews = False
        targetShift.save()
        newShiftChangeRequest.shiftsToChange.add(targetShift)
    newShiftChangeRequest.save()    

def generateSingleDayShiftDropRequest(shiftsToAddToRequest, user, reason=""):
    newShiftChangeRequest = ChangeRequest()
    newShiftChangeRequest.dateRequested = datetime.datetime.now()
    newShiftChangeRequest.requestedBy = user
    newShiftChangeRequest.requestType = 'drop'
    newShiftChangeRequest.reason = reason
    newShiftChangeRequest.startDate = MicroShift.objects.get(id=shiftsToAddToRequest[0]).date
    newShiftChangeRequest.endDate = MicroShift.objects.get(id=shiftsToAddToRequest[0]).date
    newShiftChangeRequest.status = "pending"
    newShiftChangeRequest.save()

    for eachShift in shiftsToAddToRequest:
        targetShift = MicroShift.objects.get(id=eachShift)
        targetShift.showOnNews = False
        targetShift.save()
        newShiftChangeRequest.shiftsToChange.add(targetShift)
    newShiftChangeRequest.save()

def generateMultiDayShiftDropRequest(shiftsToAddToRequest, user, endDate, repeatDays, reason=""):
    #print endDate
    daysMap = {0:'monday', 1:'tuesday', 2:'wednesday', 3:'thursday', 4:'friday', 5:'saturday', 6:'sunday'}
    targetMonth, targetDay, targetYear = endDate.split("/")
    targetEndDate = datetime.date(int(targetYear), int(targetMonth), int(targetDay))
    newShiftChangeRequest = ChangeRequest()

    newShiftChangeRequest.dateRequested = datetime.datetime.now()
    newShiftChangeRequest.requestedBy = user
    newShiftChangeRequest.requestType = 'drop'
    newShiftChangeRequest.reason = reason
    newShiftChangeRequest.startDate = MicroShift.objects.get(id=shiftsToAddToRequest[0]).date
    newShiftChangeRequest.endDate = targetEndDate
    newShiftChangeRequest.status = "pending"
    newShiftChangeRequest.save()

    
    shiftsToPutInRequest = []
    
    for eachShift in shiftsToAddToRequest:
        startingShift = MicroShift.objects.get(id=eachShift)
        shiftsToCheckForRepeat = MicroShift.objects.filter(date__gte=startingShift.date, date__lte=targetEndDate, location=startingShift.location, \
                                                       startingTime=startingShift.startingTime, endingTime=startingShift.endingTime)
        for eachRepeatingShift in shiftsToCheckForRepeat:
            if daysMap[eachRepeatingShift.date.weekday()] in repeatDays:
                if eachRepeatingShift.worker != None:
                    shiftsToPutInRequest.append(eachRepeatingShift)
    
    for eachShift in shiftsToPutInRequest:
        newShiftChangeRequest.shiftsToChange.add(eachShift)
        newShiftChangeRequest.showOnNews = True
        
    newShiftChangeRequest.save()
                
def processMultiDayShiftClaim(shiftId, user, endDate, repeatDays):
    daysMap = {0:'monday', 1:'tuesday', 2:'wednesday', 3:'thursday', 4:'friday', 5:'saturday', 6:'sunday'}
    targetMonth, targetDay, targetYear = endDate.split("/")
    targetEndDate = datetime.date(int(targetYear), int(targetMonth), int(targetDay))
    startingShift = MicroShift.objects.get(id=shiftId)
    shiftsToCheckForRepeat = MicroShift.objects.filter(date__gte=startingShift.date, date__lte=targetEndDate, location=startingShift.location, \
                                                       startingTime=startingShift.startingTime, endingTime=startingShift.endingTime)
    for eachShift in shiftsToCheckForRepeat:
        if daysMap[eachShift.date.weekday()] in repeatDays:
            if eachShift.worker == None:
              
                eachShift.worker = user
                eachShift.save()
            else:
                pass
        else:
            pass

def processMultiDayShiftDrop(shiftId, user, endDate, repeatDays):
    daysMap = {0:'monday', 1:'tuesday', 2:'wednesday', 3:'thursday', 4:'friday', 5:'saturday', 6:'sunday'}
    targetMonth, targetDay, targetYear = endDate.split("/")
    targetEndDate = datetime.date(int(targetYear), int(targetMonth), int(targetDay))
    startingShift = MicroShift.objects.get(id=shiftId)
    shiftsToCheckForRepeat = MicroShift.objects.filter(date__gte=startingShift.date, date__lte=targetEndDate, location=startingShift.location, \
                                                       startingTime=startingShift.startingTime, endingTime=startingShift.endingTime)
    
    for eachShift in shiftsToCheckForRepeat:
        if daysMap[eachShift.date.weekday()] in repeatDays:
            if eachShift.worker == user:
                eachShift.worker = None
                eachShift.showOnNews = True
                eachShift.save()
            else:
                pass
        else:
            pass
        
def processShiftDrop(shiftId, user):
    shiftToChange = MicroShift.objects.get(id=shiftId)
    if shiftToChange.worker == user:
        shiftToChange.worker = None
        shiftToChange.showOnNews = True
        shiftToChange.save()
        return "ok"
    else:

        return "notyourshift"

def processShiftAssign(shiftId, targetUser):
    shiftToChange = MicroShift.objects.get(id=shiftId)
    if targetUser == None:
        shiftToChange.worker = None
    else:
        shiftToChange.worker = targetUser
    shiftToChange.save()
    return "ok"

def processMultiDayShiftAssign(shiftId, targetUser, endDate, repeatDays):

    daysMap = {0:'monday', 1:'tuesday', 2:'wednesday', 3:'thursday', 4:'friday', 5:'saturday', 6:'sunday'}
    targetMonth, targetDay, targetYear = endDate.split("/")
    targetEndDate = datetime.date(int(targetYear), int(targetMonth), int(targetDay))
    startingShift = MicroShift.objects.get(id=shiftId)
    shiftsToCheckForRepeat = MicroShift.objects.filter(date__gte=startingShift.date, date__lte=targetEndDate, location=startingShift.location, \
                                                       startingTime=startingShift.startingTime, endingTime=startingShift.endingTime)
    for eachShift in shiftsToCheckForRepeat:
        if daysMap[eachShift.date.weekday()] in repeatDays:

            if targetUser == None:
                eachShift.worker = None
            else:
                eachShift.worker = targetUser
        else:
            pass
        eachShift.save()
    #shiftClaimEmailNotification(user.username, startDate.strftime("%m/%d/%Y"), endDate.strftime("%m/%d/%Y"), startTime.strftime("%I:%M %p"), endTime.strftime("%I:%M %p"))

def processShiftDelete(startTime, endTime, sd, location, ed, user, daysToReoccur):
    sd = sd.split("/")
    startDate = datetime.date(int(sd[2]), int(sd[0]), int(sd[1]))
    if ed:
        endDate = datetime.date(int(ed[2]), int(ed[0]), int(ed[1]))
    startTime = parseTime(startTime)

    endTime = parseTime(endTime)

    halfHour = datetime.timedelta(minutes=30)
    oneDay = datetime.timedelta(days=1)
    curTime = startTime
    if ed:
        endDate += oneDay
    error = False
    if ed:
        curDay = startDate
        shifts = MicroShift.objects.filter(location__locationName=location)
        while curDay <= endDate:
            shiftsOnThatDay = shifts.filter(date=curDay)
            curTime = startTime
            while curTime.time() != endTime.time():

                try:
                    shiftAtThatTime = shiftsOnThatDay.get(startingTime=curTime.time())
                    if shiftAtThatTime.date.weekday() in daysToReoccur:
                        shiftAtThatTime.delete()
                except:
                    pass
                curTime += halfHour
            curDay += oneDay
    else:
        shifts = MicroShift.objects.filter(location__locationName=location)
        shiftsOnThatDay = shifts.filter(date=startDate)
        while curTime.time() != endTime.time():
           try:
               shiftAtThatTime = shiftsOnThatDay.get(startingTime=curTime.time())
               shiftAtThatTime.delete()
           except:
               pass
           curTime += halfHour
    return error

def addMins(tm, min):
    fulldate = datetime.datetime(1,1,1,tm.hour,tm.minute)
    fulldate = fulldate+datetime.timedelta(minutes=min)
    return fulldate.time()
    
def getRepeatedDays(request):
    daysToRepeat = []

    if "rAll" in request.POST:
        daysToRepeat.append(dayNameToNumberMap['Monday'])
        daysToRepeat.append(dayNameToNumberMap['Tuesday'])
        daysToRepeat.append(dayNameToNumberMap['Wednesday'])
        daysToRepeat.append(dayNameToNumberMap['Thursday'])
        daysToRepeat.append(dayNameToNumberMap['Friday'])
        daysToRepeat.append(dayNameToNumberMap['Saturday'])
        daysToRepeat.append(dayNameToNumberMap['Sunday'])
    else:
        if "rMonday" in request.POST:
            daysToRepeat.append(dayNameToNumberMap['Monday'])
        if "rTuesday" in request.POST:
            daysToRepeat.append(dayNameToNumberMap['Tuesday'])
        if "rWednesday" in request.POST:
            daysToRepeat.append(dayNameToNumberMap['Wednesday'])
        if "rThursday" in request.POST:
            daysToRepeat.append(dayNameToNumberMap['Thursday'])
        if "rFriday" in request.POST:
            daysToRepeat.append(dayNameToNumberMap['Friday'])
        if "rSaturday" in request.POST:
            daysToRepeat.append(dayNameToNumberMap['Saturday'])
        if "rSunday" in request.POST:
            daysToRepeat.append(dayNameToNumberMap['Sunday'])

    return daysToRepeat

def encode_data(data):
    my_secret = 'uejsdifjqaqq'
    """Turn `data` into a hash and an encoded string, suitable for use with `decode_data`."""
    text = zlib.compress(pickle.dumps(data, 0)).encode('base64').replace('\n', '')
    m = hashlib.md5(my_secret + text).hexdigest()[:12]
    return m, text

def decode_data(hash, enc):
    """The inverse of `encode_data`."""
    my_secret = 'uejsdifjqaqq'
    text = urllib.unquote(enc)
    m = hashlib.md5(my_secret + text).hexdigest()[:12]
    if m != hash:
        raise Exception("Bad hash!")
    data = pickle.loads(zlib.decompress(text.decode('base64')))
    return data

class GMT8(datetime.tzinfo):
    def utcoffset(self, dt):
        return datetime.timedelta(hours=8, minutes=0)
    def tzname(self, dt):
        return "GMT +8"
    def dst(self, dt):
        return datetime.timedelta(0)
